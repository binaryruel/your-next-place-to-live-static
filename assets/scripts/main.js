/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages
      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired

        $('.flexslider').flexslider({
          animation: "fade"
        });

        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
          skrollr.init().destroy();
        }
        else {
          skrollr.init({
            smoothScrolling: false,
            forceHeight:false
          });
        }

        $(".navbar-toggle").on("click", function () {
          $(this).toggleClass("active");
        });

      }
    },
    // Home page
    'home': {
      init: function() {

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
        $(function initMap() {
          var myLatLng = {lat:35.0626553, lng: -80.9522546};

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatLng,
            styles:[{
              featureType: 'road',
              elementType: 'geometry',
              stylers: [

                { color: '#414142' },
                { "saturation":"-62"  },
                { "lightness":"30"  },
                { "gamma":2.5 }
              ]
            },{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ff0000"},{"saturation":43.400000000000006},{"lightness":"100"},{"gamma":1}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ff0000"},{"saturation":"-100"},{"lightness":"12"},{"gamma":1}]},{"featureType":"road.highway","elementType":"all","stylers":[{"saturation":"-62"},{"lightness":"30"},{"gamma":1},{"color":"#77777b"},{"weight":"1.16"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"saturation":"-5"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1},{"hue":"#ff0300"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","elementType":"all","stylers":[{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1},{"color":"#a5bdb0"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]}]
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: "/wp-content/themes/backett-farms/assets/images/map-icon.png"
          });
        });
      }
    },
    'faqs': {
      init: function() {

      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS
        $('#accordion').on('show.bs.collapse', function () {
          $('#accordion .in').collapse('hide');
        });
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    },
    'neighborhood':{
      init: function() {
        // JavaScript to be fired on the about us page
      },
      finalize: function(){
        $(function initMap() {
          var myLatLng = {lat:35.0626553, lng: -80.9522546};

          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 16,
            center: myLatLng,
            styles:[{
              featureType: 'road',
              elementType: 'geometry',
              stylers: [

                { color: '#414142' },
                { "saturation":"-62"  },
                { "lightness":"30"  },
                { "gamma":2.5 }
              ]
            },{"featureType":"landscape","elementType":"all","stylers":[{"hue":"#ff0000"},{"saturation":43.400000000000006},{"lightness":"100"},{"gamma":1}]},{"featureType":"poi","elementType":"all","stylers":[{"hue":"#ff0000"},{"saturation":"-100"},{"lightness":"12"},{"gamma":1}]},{"featureType":"road.highway","elementType":"all","stylers":[{"saturation":"-62"},{"lightness":"30"},{"gamma":1},{"color":"#77777b"},{"weight":"1.16"}]},{"featureType":"road.highway","elementType":"labels.text","stylers":[{"saturation":"-5"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"all","stylers":[{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1},{"hue":"#ff0300"}]},{"featureType":"road.local","elementType":"all","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","elementType":"all","stylers":[{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1},{"color":"#a5bdb0"}]},{"featureType":"water","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"}]}]
          });

          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            icon: "/wp-content/themes/backett-farms/assets/images/map-icon.png"
          });
        });
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };

  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
