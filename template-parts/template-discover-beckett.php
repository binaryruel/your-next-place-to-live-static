<div class="below-highlited-feature">
    <div class="container">

        <?php if( is_page( array( 'amenities','gallery' ) ) ) { ?>
            <img src="/wp-content/themes/backett-farms/assets/images/subpage-icon.png" alt="">
        <?php } ?>

        <p>Live in harmony with nature at Beckett Farms, where contemporary design and luxury amenities blend with the natural environment.</p>
        <h3>Discover what makes Beckett Farms so unique</h3>
        <div class="row">
            <div class="col-md-4 col-sm-4">
                <a href="#" class="hf-button"><span>GALLERY</span></a>
            </div>
            <div class="col-md-4 col-sm-4">
                <a href="#" class="hf-button"><span>NEIGHBORHOOD</span></a>
            </div>
            <div class="col-md-4 col-sm-4">
                <a href="#" class="hf-button"><span>FAQs</span></a>
            </div>
        </div>
    </div>
</div>