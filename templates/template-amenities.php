<?php
/**
 * Template Name: Amenities Template
 */
?>
<div id="amenities">
    <div class="banner subpage">
        <div class="brown-container"></div>
        <div class="visible-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:350%;
    top:-100px;
 "
             data-bottom-top="transform: translate3d(0px, -90%, 0px);" data-top-bottom="transform: translate3d(0px, 0px, 0px);"
        ></div>
        <div class="hidden-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:100%;
 "
        >
        </div>
    </div>
    <div class="amenities-container subpage-container">
        <div class="container">
            <h1>DIVE IN</h1>
            <p>Beckett Farms will offer the finest in luxury amenities for residents to enjoy.</p>

            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="amenities-item">
                            <div class="image-container">
                                <img src="/wp-content/themes/backett-farms/assets/images/amenities/ladder.png" alt="">
                            </div>
                            <p>
                                Saltwater/mineral pool <br> with sun shelves
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="amenities-item">
                            <div class="image-container">
                                <img src="/wp-content/themes/backett-farms/assets/images/amenities/house.png" alt="">
                            </div>
                            <p>
                                Private covered <br> outdoor cabanas
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="amenities-item">
                            <div class="image-container">
                                <img src="/wp-content/themes/backett-farms/assets/images/amenities/summer.png" alt="">
                            </div>
                            <p>
                                Lounge chairs, and tables and <br> chairs with umbrellas
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/ramen.png" alt="">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/ramen-hover.png" class="onhover" alt="">
                        </div>
                        <p>
                            Outdoor kitchen <br> and grill station
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/coffe.png" alt="">
                        </div>
                        <p>
                            6,000-square-foot <br> clubhouse with a cyber caf&eacute;
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/fire.png" alt="">
                        </div>
                        <p>
                            2 fire pit <br> lounge areas
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/chair-tree.png" alt="">
                        </div>
                        <p>
                            Pocket parks and play lawns <br> with benches, tables and <br> swings
                        </p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/barbel.png" alt="">
                        </div>
                        <p>
                            State-of-the-art fitness <br> center with a wide array of <br> equipment as well as yoga, <br> spin and virtual fitness room
                        </p>
                    </div>
                </div>

                <div class="col-md-4 col-sm-4">
                    <div class="amenities-item">
                        <div class="image-container">
                            <img src="/wp-content/themes/backett-farms/assets/images/amenities/trees.png" alt="">
                        </div>
                        <p>
                            Woodland protected areas <br> surrounding the property, <br> providing privacy and security
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="gallery">
        <div class="col-md-12">
            <div class="row">
                <a href="/wp-content/themes/backett-farms/assets/images/shutterstock_33982567.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/shutterstock_33982567.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/Girl_Food_and_Wine_Tour.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Girl_Food_and_Wine_Tour.jpg);
            background-position:20% 25%;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0416.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0416.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0350.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0350.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/shutterstock_40277635.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/shutterstock_40277635.jpg);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0432_edit.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0432_edit.jpg);
            background-position:20% 25%;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0463.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0463.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <?php
            get_template_part('template-parts/template','discover-beckett');
            ?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <?php
            get_template_part('template-parts/template','bottom-banner');
            ?>
        </div>
    </div>
</div>

