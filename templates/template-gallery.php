<?php
/**
 * Template Name: Gallery Template
 */
?>
<div id="gallery">
    <div class="gallery-container subpage-container">
        <div class="container">
            <h1>Where the Great Outdoors <br> Meets the Luxurious Indoors</h1>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/Beckett Farms Final Renderings_low res_5.12.16-4.jpg" data-lightbox="image-1" data-title="My caption">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-4.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0432_edit.jpg" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0432_edit.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0515.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0515.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0463.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0463.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0416.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0416.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/Beckett Farms Final Renderings_low res_5.12.16.jpg" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/Beckett Farms Final Renderings_low res_5.12.16-2.jpg" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0350.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0350.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="gallery-item">
                            <a href="/wp-content/themes/backett-farms/assets/images/Beckett Farms Final Renderings_low res_5.12.16-3.jpg" data-lightbox="image-1">
                                <div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-3.jpg);
            background-position:40% 30%;
            -webkit-background-size: cover;
            background-size: cover;"></div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
        <?php
            get_template_part('template-parts/template','discover-beckett');
        ?>
        </div>
    </div>
</div>

