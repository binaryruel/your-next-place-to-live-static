<?php
/**
 * Template Name: Homepage Template
 */
?>
<div id="homepage">
    <div class="flexslider">
        <ul class="slides">
            <li class="item">
                <div
                    class="banner visible-lg"
                    style="
        background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
        background-position:20% 365%;
        -webkit-background-size: cover;
        background-size: cover;
        height:300%;
    " data-bottom-top="transform: translate3d(0px, -45%, 0px);" data-top-bottom="transform: translate3d(0px, 45%, 0px);">

                </div>
                <div class="hidden-lg" style="
                    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
                    background-position:63% 65%;
                    -webkit-background-size: cover;
                    background-size: cover;
                    height:100%;"
                ></div>
            </li>
            <li class="item">
                <div
                    class="banner visible-lg"
                    style="
        background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-3.jpg);
        background-position: 20% 90%;
        top: -50%;
        -webkit-background-size: cover;
        background-size: cover;
        height:200%;
    " data-bottom-top="transform: translate3d(0px, -45%, 0px);" data-top-bottom="transform: translate3d(0px, 45%, 0px);">

                </div>
                <div class="hidden-lg" style="
                    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-3.jpg);
                    background-position:20% 65%;
                    -webkit-background-size: cover;
                    background-size: cover;
                    height:100%;"
                ></div>
            </li>
            <li class="item">
                <div
                    class="banner visible-lg"
                    style="
        background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-4.jpg);
        background-position:20% 165%;
        -webkit-background-size: cover;
        background-size: cover;
        height:300%;
    " data-bottom-top="transform: translate3d(0px, -45%, 0px);" data-top-bottom="transform: translate3d(0px, 45%, 0px);">

                </div>
                <div class="hidden-lg" style="
                    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-4.jpg);
                    background-position:20% 65%;
                    -webkit-background-size: cover;
                    background-size: cover;
                    height:100%;"
                ></div>
            </li>
        </ul>
    </div>
    <div class="bottom-banner">
        <div class="container">
            <h1>RUSTIC-INSPIRED, URBAN CHIC</h1>
            <p>Beckett Farms will offer upscale apartment living with a modern farmhouse style on 43 verdant acres in Fort Mill, S.C.</p>
            <a class="signup-button" data-scroll href="#signup">STAY CONNECTED</a>
        </div>
    </div>
    <div class="highlighted-features">
        <div class="container">
            <h3>HIGHLIGHTED FEATURES:</h3>
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="feature-item">
                        <div class="head-image" style="
                            background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-3.jpg);
                            background-position:20% 68%;

                        "></div>
                        <div class="detail">
                            <h4 class="title">INSPIRED INTERIORS</h4>
                            <p>Our apaertments include upgraded wood-style flooring, farmhouse-inspired fixtures and lighting, and washers and dryers.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="feature-item">
                        <div class="head-image" style="
                            background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-4.jpg);
                            background-position:20% 68%;

                        "></div>
                        <div class="detail">
                            <h4 class="title">GOURMENT KITCHENS</h4>
                            <p>Kitchens feature stainless steel appliances, granite countertops, freestanding island and tiled backsplashes.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="feature-item">
                        <div class="head-image" style="
                            background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16-2.jpg);
                            background-position:20% 68%;

                        "></div>
                        <div class="detail">
                            <h4 class="title">AMPLE AMENITIES</h4>
                            <p>Relax by the saltwater pool, work out the state-of-the-art fitness center or grill in the outdoor kitchen.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
        get_template_part('template-parts/template','discover-beckett');
    ?>
    <div class="banner">
        <div class="visible-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16.jpg);
    background-position:center;
    -webkit-background-size: cover;
    background-size: cover;
    height:330%;
    top:-20%;
 "
             data-bottom-top="transform: translate3d(0px, -60%, 0px);" data-top-bottom="transform: translate3d(0px, 60%, 0px);"
        ></div>
        <div class="hidden-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/Beckett%20Farms%20Final%20Renderings_low%20res_5.12.16.jpg);
    background-position:20% 60%;
    -webkit-background-size: cover;
    background-size: cover;
    height:100%;
 "
        >

        </div>
    </div>
    <div class="address">
        <div class="col-md-6">
            <div class="row">
                <div class="left">
                    <h3>HIDDEN AWAY</h3>
                    <p>Located off Gold Hill Road in Fort Mill, Beckett Farm is located in the coveted Fort Mill School District
                    width easy access to I-77 and key employment centers, including Ballantyne, SouthPark and Uptown Charlotte. Residents
                    will enjoy high-quality retail and dining nearby at Springfield Town Center, Kingsley and Baxter Village. Anne Springs Close Greenway, which offers 2,100 acres
                    of outdoor recreation and 40 miles of trails, is just five minutes away.</p>
                    <img src="/wp-content/themes/backett-farms/assets/images/location-icon-bottom.png" alt="">
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="row">
                <div class="right">
                    <div id="map"></div>
                    <span class="label">1111 GENNETT CIRCLE, FORT MILL, SC</span>
                </div>
            </div>
        </div>
    </div>
    <div class="below-address-gallery">
        <div class="col-md-12">
            <div class="row">
                <a href="/wp-content/themes/backett-farms/assets/images/shutterstock_40277635.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/shutterstock_40277635.jpg);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0404.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0492.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/Girl_Food_and_Wine_Tour.jpg" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/Girl_Food_and_Wine_Tour.jpg);
            background-position:20% 25%;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
                <a href="/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG" data-lightbox="image-1"><div class="mini-image" style="background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0348.JPG);
            background-position:center;
            -webkit-background-size: cover;
            background-size: cover;"></div></a>
            </div>
        </div>
    </div>
</div>

