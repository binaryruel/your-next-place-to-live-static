<?php
/**
 * Template Name: Neighborhood Template
 */
?>
<div id="neighborhood">
    <div class="banner subpage">
        <div class="brown-container"></div>
        <div class="visible-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0547.JPG);
    background-position:50% 70%;
    -webkit-background-size: cover;
    background-size: cover;
    height:350%;
    top:-100px;
 "
             data-bottom-top="transform: translate3d(0px, -90%, 0px);" data-top-bottom="transform: translate3d(0px, 0px, 0px);"
        ></div>
        <div class="hidden-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0547.JPG);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:100%;
 "
        >
        </div>
    </div>
    <div class="neighborhood-container subpage-container">
        <div class="container">
            <h1>NEIGHBORHOOD</h1>
            <p>Enjoy the serenity and privacy of living in a natural setting while also experiencing all of the luxuries of urban life. In addition to being surrounded by trees, Beckett Farms is located in one of the region’s best school districts, minutes away from unique restaurants and great places to take a daytrip. With all of the amenities you need both at Beckett Farms and in the surrounding area, you won’t ever be traveling too far from home. </p>

        </div>
    </div>
    <div class="neighborhood-content">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="image-side">
                            <div class="banner ">
                                <div
                                    style="
                                    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0350.JPG);
                                    background-position:center;
                                    -webkit-background-size: cover;
                                    background-size: cover;
                                    height:100%;
                                 ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                        <div class="info-side">
                            <h4>Fort Mill Features</h4>
                            <ul class="parent-list">
                                <li>Historic Downtown Fort Mill</li>
                                <li>Walter Y. Elisha Park
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <ul class="2-col">
                                                        <li>- playground</li>
                                                        <li>- walking paths</li>
                                                    </ul>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-12">
                                                <div class="row">
                                                    <ul>
                                                        <li>- paved bike trails</li>
                                                        <li>- sports fields</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                </li>
                                <li>Anne Springs Close Greenway
                                    <ul>
                                        <li>- 2,100 acres</li>
                                        <li>- 40 miles of trails</li>
                                    </ul>
                                </li>
                                <li>South Carolina Strawberry Festival (May)</li>
                                <li>Baxter Village St. Patrick's Day Celebration</li>
                            </ul>
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                        <div class="info-side short-content">
                            <h4>Restaurants Nearby</h4>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <ul class="parent-list">
                                        <li>McAlister's Deli</li>
                                        <li>Six Pence Pub</li>
                                        <li>Hobo's</li>
                                        <li>Captain Steve's</li>
                                        <li>Fish Market</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="row">
                                    <ul class="parent-list">
                                        <li>Carmella's</li>
                                        <li>The Flipside Cafe</li>
                                        <li>Fort Mill BBQ Company</li>
                                        <li>Local Dish Restaurant</li>
                                        <li>Brixx (coming soon!)</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="banner ">
                            <div
                                 style="
                                    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0416.JPG);
                                    background-position:center;
                                    -webkit-background-size: cover;
                                    background-size: cover;
                                    height:100%;
                                 ">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">
                    <div class="row">
                        <div class="image-side">
                            <div class="banner ">
                                <div
                                    style="
                                    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0463.JPG);
                                    background-position:center;
                                    -webkit-background-size: cover;
                                    background-size: cover;
                                    height:100%;
                                 ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">

                        <div class="info-side short-content">
                            <h4>Local Schools</h4>
                            <ul class="parent-list">
                                <li>Springfield Elementary School District</li>
                                <li>Springfield Middle School Distrcit</li>
                                <li>Nation Ford High School District</li>
                                <li>The Goddard School</li>
                                <li>Fort Mill Preparatory Cooperative Preschool</li>
                            </ul>
                        </div>

                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6">

                        <div class="info-side short-content">
                            <h4>Within Driving Distance</h4>
                            <ul class="parent-list">
                                <li>Anne Springs Close Greenway - 5 mins</li>
                                <li>McDowell Nature Preserve - 10 mins</li>
                                <li>Carowinds - 10 mins</li>
                                <li>Riverwalk - 12 mins</li>
                                <li>Museum of York County - 20 mins</li>
                                <li>Baxter Village St. Patrick's Day Celebration - 25 mins</li>
                            </ul>
                        </div>

                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="image-side">
                            <div class="banner ">
                                <div
                                    style="
                                    background-image: url(/wp-content/themes/backett-farms/assets/images/POP_3123.JPG);
                                    background-position:center;
                                    -webkit-background-size: cover;
                                    background-size: cover;
                                    height:100%;
                                 ">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="bottom-map">
        <div class="col-md-12">
            <div class="row">
                <div class="info-container">
                    <span>COME SEE US! </span> <br>
                    1111 GENNETT CIRCLE FORT MILL,SC
                </div>
                <div id="map"></div>
            </div>
        </div>
    </div>
</div>

