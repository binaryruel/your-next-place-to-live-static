<footer class="footer">
  <div class="col-md-12" id="signup">
    <div class="row">
      <div class="stay-connected">
        <div class="container">
          <h3>STAY CONNECTED</h3>
          <p>Keep up with the latest news about Beckett Farms.</p>
          <input type="text" placeholder="First Name">
          <input type="text" placeholder="Last Name">
          <input type="text" placeholder="Phone Number">
          <input type="email" placeholder="Email">
          <button>SUBMIT</button>
        </div>
      </div>
      <div class="footer-address">
        <div class="container">
          <div class="social-media">
            <ul>
              <li><img src="http://placehold.it/50x50" alt="" class="img-circle"></li>
              <li><img src="http://placehold.it/50x50" alt="" class="img-circle"></li>
              <li><img src="http://placehold.it/50x50" alt="" class="img-circle"></li>
            </ul>
          </div>
          <div class="info">
            <ul>
              <li>HOME</li>
              <li>NEIGHBORHOOD</li>
              <li>AMENITIES</li>
              <li>PHOTOS</li>
              <li>FAQs</li>
              <li>CONTACT</li>
            </ul>
            <p>1111 GENNETT CIRCLE, FORT MILL,SC</p>
            <p>&copy; 2016 Backett Farms | Charlotte Web Design</p>
          </div>
        </div>
        <?php if( is_page( array( 'faqs','amenities' ) ) ) { ?>
          <div class="faq-footer">
            <ul>
              <li><a href="#"><img src="/wp-content/themes/backett-farms/assets/icons/Footer/EHO_Logo-01.png" alt=""></a></li>
              <li><a href="#"><img src="/wp-content/themes/backett-farms/assets/icons/Footer/ADA_Logo-01.png" alt=""></a></li>
            </ul>
          </div>
        <?php } ?>
      </div>
    </div>
  </div>
</footer>
