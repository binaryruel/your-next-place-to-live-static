<?php
/**
 * Template Name: FAQs Template
 */
?>
<div id="faqs">
    <div class="banner subpage">
        <div class="brown-container"></div>
        <div class="visible-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0507.JPG);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:350%;
    top:-100px;
 "
             data-bottom-top="transform: translate3d(0px, -90%, 0px);" data-top-bottom="transform: translate3d(0px, 0px, 0px);"
        ></div>
        <div class="hidden-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0507.JPG);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:100%;
 "
        >
        </div>
    </div>
    <div class="faqs-container subpage-container">
        <div class="container">
            <h1>FAQs</h1>
            <p>Backett Farms will offer the finest in luxury amenities for residents to enjoy.</p>
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel-left col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                        What time is the pool open until? The gym? Are guests allowed to use theses facilities?
                                </h4>
                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                            <div class="clear"></div>
                        </a>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <div class="panel-heading" role="tab" id="headingTwo">
                                <h4 class="panel-title">
                                        What types of apartments do you offer? Are the amenities in each apartment customizable?
                                </h4>

                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                            <div class="panel-heading" role="tab" id="headingThree">
                                <h4 class="panel-title">
                                       Does the apartment complex offer package handling services? Hoe do I receive a package sent to my apartment?
                                </h4>
                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                            <div class="panel-heading" role="tab" id="headingFour">
                                <h4 class="panel-title">
                                        Quisque in auctor lectus?
                                </h4>
                                   <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-right col-md-6 col-sm-6">
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne2" aria-expanded="true" aria-controls="collapseOne2">
                            <div class="panel-heading" role="tab" id="headingOne2">
                                <h4 class="panel-title">
                                        If there are any issues with our apartment, who do we contact? When are they available to contact?
                                </h4>

                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseOne2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne2">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2">
                            <div class="panel-heading" role="tab" id="headingTwo2">
                                <h4 class="panel-title">
                                        How do residents stay up to date with the latest news about Backett Farms?
                                </h4>
                                   <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseTwo2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo2">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2">
                            <div class="panel-heading" role="tab" id="headingThree2">
                                <h4 class="panel-title">
                                       Are pets Allowed? What's your policy?
                                </h4>
                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseThree2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree2">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour2" aria-expanded="false" aria-controls="collapseFour2">
                            <div class="panel-heading" role="tab" id="headingFour2">
                                <h4 class="panel-title">
                                       Ultricies mi quis est molestie rutrum, nullam quis elit
                                </h4>
                                <img src="/wp-content/themes/backett-farms/assets/images/accordion-collapse.png" alt="">
                            </div>
                        </a>
                        <div id="collapseFour2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour2">
                            <div class="panel-body">
                                Fusce ullamcorper sem sapien, eu dictum mi tempus et. In fringilla a enim non fringilla. In ultricies mi quis est molestie rutrum. Nullam quis elit in risus facilisis pharetra. Sed ut tortor in sem ultricies auctor.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="get-in-touch">

        <p>Have more questions for us? We'd love to hear frome you!</p>
        <h4>Contact Beckett Farms for any questions not listed here</h4>
        <a href="#"> GET IN TOUCH</a>
    </div>
    <?php
        get_template_part('template-parts/template','bottom-banner');
    ?>
</div>

