<header class="header">

    <div class="social-media">
      <div class="container">
        <span>CONTACT US TODAY! 123-456-789</span>
        <ul>
          <li><img src="http://placehold.it/15x15" alt="" class="img-circle"></li>
          <li><img src="http://placehold.it/15x15" alt="" class="img-circle"></li>
          <li><img src="http://placehold.it/15x15" alt="" class="img-circle"></li>
        </ul>
      </div>
    </div>
    <div class="logo">
      <img src="/wp-content/themes/backett-farms/assets/images/logo.png" alt="">
      <nav class="navbar navbar-default">
        <div class="container">
          <div class="navbar-header">
            <span class="label">MENU</span>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active"><a href="#">HOME</a></li>
              <li><a href="#">NEIGHBORHOOD</a></li>
              <li><a href="#">AMENITIES</a></li>
              <li><a href="#">GALLERY</a></li>
              <li><a href="#">FAQs</a></li>
              <li><a href="#">CONTACT</a></li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
    </div>
</header>
