<?php
/**
 * Template Name: Contact Us Template
 */
?>
<div id="contact-us">
    <div class="banner subpage">
        <div class="brown-container"></div>
        <div class="visible-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0453.JPG);
    background-position:50% 75%;
    -webkit-background-size: cover;
    background-size: cover;
    height:350%;

 "
             data-bottom-top="transform: translate3d(0px, -90%, 0px);" data-top-bottom="transform: translate3d(0px, 0px, 0px);"
        ></div>
        <div class="hidden-lg"
             style="
    background-image: url(/wp-content/themes/backett-farms/assets/images/DSC_0453.JPG);
    background-position:bottom;
    -webkit-background-size: cover;
    background-size: cover;
    height:100%;
 "
        >
        </div>
    </div>
    <div class="form-container subpage-container">
        <div class="container">

            <h1>Contact Us</h1>
            <p>Complete the form below to get the latest leasing news and updates on Beckett Farms.</p>

            <div class="col-md-12">
                <div class="col-md-6 col-sm-6">
                    <input type="text" placeholder="First Name" name="">
                </div>
                <div class="col-md-6 col-sm-6">
                    <input type="text" placeholder="Last Name" name="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 col-sm-6">
                    <input type="text" placeholder="Street Address" name="">
                </div>
                <div class="col-md-6 col-sm-6">
                        <input type="text" placeholder="City" name="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-6 col-sm-6">
                        <div class="col-md-6 col-sm-6 padding-left-0">

                                <input type="text" placeholder="State" name="">

                        </div>
                        <div class="col-md-6 col-sm-6 padding-right-0">

                                <input type="text" placeholder="Zip Code" name="">

                        </div>
                </div>
                <div class="col-md-6 col-sm-6">
                        <input type="text" placeholder="City" name="">
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                    <textarea name="" id="" cols="30" rows="10" placeholder="How can we help you today?"></textarea>

                    <button class="contact-button">SUBMIT</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
        <?php
            get_template_part('template-parts/template','discover-beckett');
        ?>
        </div>
    </div>
</div>

